FROM nginx

LABEL MANTAINER="Flávio Lopes <flavio@links.inf.br>"
LABEL APP_VERSION="1.0.2"

RUN apt-get update && apt-get install -y vim

WORKDIR /usr/share/nginx/html

ADD ./atualizacao.tar.gz ./

